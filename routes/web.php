<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', 'RegistrationController@create');
Route::get('/profile', 'RegistrationController@edit');
Route::post('register', 'RegistrationController@store');
//Route::put('register', 'RegistrationController@update');

Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');
 
Route::get('/add', 'Calendar@create');
Route::post('/add', 'Calendar@store');
Route::post('/update', 'Calendar@update');
Route::match(['get', 'post'], '/data/{id}', 'Calendar@data');
Route::match(['get', 'post'], '/delete/{id}', 'Calendar@deleteEvent');
Route::get('/calendar/{id}', 'Calendar@show');

/*Route::get('/calendar', function () {
    return view('calendar');
});*/

Route::resource('calendar','Calendar');
