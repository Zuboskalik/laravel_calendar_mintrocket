<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Добавление нового события</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
		
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .textfield {
                text-align: center;
                margin-bottom: 15px;	
				max-width: 750px;
			}
			
            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>

	  <!-- import CSS -->
	</head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
				
						<div id="links">
				            @if( auth()->check() )
								<div class="title m-b-md">
									Здравствуйте, {{ auth()->user()->name }}#{{ auth()->user()->id }} 
								</div>
								<div>
									<div class="links">
										<div class="textfield">Создание нового события</div>									
										<br>
										<div class="textfield">описание:<el-input v-model="title"></el-input></div>
										<br>
										<div class="textfield">дата:<el-date-picker type="date" locale="ru" format="dd.MM.yyyy" value-format="yyyy-MM-dd HH:mm:ss" v-model="date_start"></el-date-picker></div>
										<br>
										<div class="textfield">повторять еженедельно:<el-switch v-model="repeat"></el-switch></div>
									<br>
									
										<div style="font-size: 20px">
										@{{ title || 'описание не указано' }}<br>
										@{{ date_start || 'дата не выбрана' }}<br>										
										@{{ repeat?'повторяемое':'не повторяется' }}<br>
										</div>
										<hr>
										<el-button  v-on:click="add()">добавить</el-button>
										
										<el-button  v-on:click="open('/calendar')">Список событий</el-button>
										<!--<a href="{{ url('/calendar') }}">Список событий</a>-->
									</div>
									
								</div>
							@else
								<div class="title m-b-md">
									Ошибка: вы должны быть зарегистрированы для добавления события!
									
							<div class="form-group">
								<div class="links">
									<br>
									<el-button  v-on:click="open('/login')">Войти на сайт</el-button>
									<el-button  v-on:click="open('/register')">Регистрация</el-button>
									<!--<a href="{{ url('/login') }}">Войти на сайт</a>
									<a href="{{ url('/register') }}">Регистрация</a>-->									
								</div>							
							</div>
							@endif
								<div class="links">
									<el-button  v-on:click="open('/')">Главная страница</el-button>
									<!--<br><a href="{{ url('/') }}">Главная страница</a>-->	
								</div>
						</div>
							

            </div>
        </div>
		
		  <div id="app">
			<el-button @click="visible = true">Описание Frontend-части</el-button>
			<el-dialog :visible.sync="visible" title="Выполнено с помощью ElementUI">
			  <p>Element, a Vue 2.0 based component library for developers, designers and product managers</p>
			</el-dialog>
		  </div>
		  
    </body>
	
  <!-- import Vue before Element -->
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <!-- import JavaScript -->
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
	
		  <script>
			new Vue({
			  el: '#app',
			  data: function() {
				return { visible: false }
			  }
			})
		  </script>	

		  <script>
		  
		  
			new Vue({
			  el: '#links',
			  data: {
					ajaxRequest: false,
					title: '',
					date_start: '',
					repeat: false
				  },
			  methods: {
				open: function (message) {
				  window.location.href = message
				},				
				add: function () {						
					$.post( "/add", { 								  
						title: this.title,
						user_id: {{auth()->user()->id}},
						date_start: this.date_start,
						repeat: this.repeat?1:0, 
						_token: <?php echo "'".csrf_token()."'"; ?>, 
						
					  } ).done(function( dat ) {
						window.location.href = '/calendar/' + dat ;
					  });						
				}
			  }
			})
		  </script>
</html>







