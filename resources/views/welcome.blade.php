<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 54px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
				
				            @if( auth()->check() )
								<div class="title m-b-md">
									Здравствуйте, {{ auth()->user()->name }}
								</div>
							@else
								<div class="title m-b-md">
									Здравствуйте, проверяющий мою работу для Mint Rocket
								</div>
							@endif

                <div id="links">
					<div class="links">
						@if (Auth::check())
							<!--<a href="{{ url('/calendar') }}">Календарь</a>
							<a href="{{ url('/profile') }}">Обновить профиль</a>
							<a href="{{ url('/logout') }}">Выйти</a>-->
							
							<el-button  v-on:click="open('/calendar')">Календарь</el-button>
							<el-button  v-on:click="open('/add')">Добавить событие</el-button>
							<!--<el-button  v-on:click="open('/profile')">Обновить профиль</el-button>-->
							<el-button  v-on:click="open('/logout')">Выйти</el-button>
						@else
							<!--<a href="{{ url('/login') }}">Войти на сайт</a>
							<a href="{{ url('/register') }}">Регистрация</a>-->
							
							<el-button  v-on:click="open('/login')">Войти на сайт</el-button>
							<el-button  v-on:click="open('/register')">Регистрация</el-button>
						@endif
					</div>
                </div>
            </div>
        </div>
    </body>
			  
  <!-- import Vue before Element -->
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <!-- import JavaScript -->
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
		
	<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
			  
		<script>
			new Vue({
			  el: '#links',
			  methods: {
				open: function (message) {
				  window.location.href = message
				}
			  }
			})
		</script>	
</html>
