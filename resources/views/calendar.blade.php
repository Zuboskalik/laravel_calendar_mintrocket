<!doctype html>
<html lang="en">
<head>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.27/vue.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
	
	<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
	<script src="https://unpkg.com/element-ui/lib/index.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/lang-all.js"></script>
		
    <style>	
            .panel-heading {
                position: absolute;
                left: 10px;
                top: 18px;		
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;		
			}
            .page {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .page > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }            
			.calendar {
                font-family: 'Raleway', sans-serif;
                width:55%; 
				text-align:center;
				margin: 40px auto;
				padding: 0 10px;
            }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">


                <div class="panel-body">
                    @if (Auth::check())
						<div class="panel-heading">
							Тестовая работа "календарь"
							<br>
							<a href="{{ url('/') }}">главная страница</a>
						</div>
						<div class="calendar">
                    	{!! $calendar->calendar() !!}
						{!! $calendar->script() !!}
						</div>
                    @else
						<div class="page">
					Пожалуйста, 
                        <a href="{{ url('/login') }}">войдите на сайт</a> или 
                        <a href="{{ url('/register') }}">зарегистрируйтесь</a>
						</div>
						
                    @endif
                </div>
				

            </div>
        </div>
    </div>
</div>
	
</body>


	
</html>