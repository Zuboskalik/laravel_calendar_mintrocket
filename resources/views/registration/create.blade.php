<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
				
								<div class="title m-b-md">
									Введите данные для регистрации
								</div>


								<div id="app">
									<h2>Регистрация</h2>
									<form method="POST" action="/register">
										{{ csrf_field() }}
										<div class="form-group">
											<label for="name">Имя:</label>
											<input  v-model="message" type="text" class="form-control" id="name" name="name">	</input>			
										</div>
								 
										<div class="form-group">
											<label for="email">Email:</label>
											<input type="email" class="form-control" id="email" name="email"></input>
										</div>
								 
										<div class="form-group">
											<label for="password">Пароль:</label>
											<input type="password" class="form-control" id="password" name="password"></input>
										</div>
								 
										<div class="form-group">
											<button style="cursor:pointer" type="submit" class="btn btn-primary">Регистрация</button>
										</div>
										<br>
										<div class="form-group">
											<el-button v-on:click="open('/')">Отмена</el-button>
										</div>
									</form>
								</div>
				
				
            </div>
        </div>
    </body>
	
  <!-- import Vue before Element -->
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <!-- import JavaScript -->
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
		<link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
	
		  <script>
			new Vue({
			  el: '#app',
			  methods: {
				open: function (message) {
				  window.location.href = message
				}
			  }
			})
		  </script>
	
</html>



