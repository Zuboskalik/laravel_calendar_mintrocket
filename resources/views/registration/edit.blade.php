<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
				
								<div class="title m-b-md">
									Введите данные для входа
								</div>


								<div id="app">
									<h2>Изменение данных пользователя {{ auth()->user()->name }}</h2>
									<form method="POST" action="/register">
										{{ csrf_field() }}
									
											<input type="hidden" name="_method" value="PUT">
											<input  v-model="message" type="hidden" class="form-control" id="id" name="id">				
									
										<div class="form-group">
											<label for="name">Имя:</label>
											<input  v-model="message" type="text" class="form-control" id="name" name="name">				
										</div>
								 
										<div class="form-group">
											<label for="email">Email:</label>
											<input type="email" class="form-control" id="email" name="email">
										</div>
								 
										<div class="form-group">
											<label for="password">Пароль:</label>
											<input type="password" class="form-control" id="password" name="password">
										</div>
								 
										<div class="form-group">
											<button style="cursor:pointer" type="submit" class="btn btn-primary">Отправить</button>
										</div>
									</form>
								</div>
				
				
            </div>
        </div>
    </body>
</html>



