<?php

namespace App\Http\Controllers;

use App\EventModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MaddHatter\LaravelFullcalendar\Event;



class Calendar extends Controller{
		
protected $defaultOptions = [
	'header' => [
	'left' => 'prev,next today',
	'center' => 'title',
	'right' => 'month,agendaWeek,agendaDay',
	],
	'eventLimit' => true,
	'lang' => 'ru'
	];
	
	public function data($id){		
		$event = EventModel::find($id);
		
		return $event;		
	}	
	
	public function deleteEvent($id){		
		$event = EventModel::destroy($id);
		
		return 'deleted';		
	}	
	
	public function show($id){		
		$event = EventModel::find($id);
		
		return view('event', compact('event'));		
	}
	
    public function create()
    {
        return view('addevent');
    }    
	
	public function store(request $request)
    {
		//$token = request(['_token']);
         $this->validate(request(), [
            'title' => 'required',
            'date_start' => 'required',
        ]);
        
        $event = EventModel::create(request(['title', 'user_id', 'date_start', 'repeat']));
        
        //return redirect()->to('/calendar/'.$event->id);
        return $event->id;
    }	
	
	public function update(request $request)
    {
		//$token = request(['_token']);
         $this->validate(request(), [
            'title' => 'required',
            'date_start' => 'required',
        ]);
		
        $event = EventModel::Find(request(['title']));
		
        $event = EventModel::update(request(['title', 'user_id', 'date_start', 'repeat']));
        
        //return redirect()->to('/calendar/'.$event->id);
        return $event->id;
    }
	
public function index()
    {
		$events = [];

		$event = EventModel::all();

		foreach ($event as $eve) {
		  $events[] = \Calendar::event(
		  $eve->title, //event title
		  $eve->full_day, //весь день?
		  $eve->date_start, //start time (you can also use Carbon instead of DateTime)
		  $eve->date_start, //end time (you can also use Carbon instead of DateTime)
		  $eve->id, //ID
			[
					'dow' => ($eve->repeat==1)?[date('N', strtotime($eve->date_start))]:'',			//повторения		
			]
		  );
		};

		//тесты
		/*
		$event_id = 'stringEventId';
		$events[] = \Calendar::event(
			"раз в неделю!", //название
			true, //идет ли весь день
			new \DateTime('2018-03-27'), //от (Carbon или DateTime)
			new \DateTime('2018-03-27'), //до 
			$event_id, //ID
			[
					'dow' => [date('N', strtotime('2018-03-27'))],					
			]
		);*/	
		/*
		$eloquentEvent = EventModel::first(); //EventModel implements MaddHatter\LaravelFullcalendar\Event
				if (isset($eloquentEvent))
		$calendar = \Calendar::addEvent($eloquentEvent, [ //цвет
				'color' => '#800',
			])->setOptions([ //опции
				'firstDay' => 1
			])->setCallbacks([ //событие на клик
				'viewRender' => 'function() {alert("пинг");}'
			]);
		*/	 
		 
		$calendar = \Calendar::addEvents($events)
		->setCallbacks([ //кликабельно для пользователей
			'eventClick' => "function(calEvent, jsEvent, view) {
					window.location = '/calendar/' + calEvent.id;
				}"
			]);//список событий
		

		return view('calendar', compact('calendar'));

    }
	

}
