<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\User;
 
class RegistrationController extends Controller
{
    public function create()
    {
        return view('registration.create');
    }
    
    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);
        
        $user = User::create(request(['name', 'email', 'password']));
        
        auth()->login($user);
        
        return redirect()->to('/');
    }    
	
	public function edit()
    {
        return view('registration.edit');
    }
	
	public function update($id, UserFormRequest $request)
    {
		$user = User::findOrFail($id);

		$user->name = $request->get('name');

		$user->email = $request->get('email');

		$user->save();
                
        auth()->login($user);
        
        return redirect()->to('/');
    }
}